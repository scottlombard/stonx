import yfinance as yf
import json
import csv
import MySQLdb
import pandas as pd
import os
from csv import reader
from csv import writer
from tabulate import tabulate
from datetime import datetime
from pandas_datareader import data as pdr

#global variable for start of program
startTime = datetime.now()
#This will be inaccurate under multiple iterations, add to dataDownload()

def setTicker():
    ticker = "MSFT" #eventually read from a file
    return ticker

def dataDownload(str):
    # Download the data, export to csv file
    ticker = str
    yf.pdr_override()
    data = yf.download(tickers="MSFT", period="5d", interval="60m") #change to dataDownload(str) and tickers = ticker
    pd.DataFrame(data)
    print('DataFrame:\n', data)
    csv_data = data.to_csv('/home/pi/Projects/stonx/Data/MSFT.csv', index = True, header = False) 
    
def runTime():
    print("Start: ")
    print(startTime)
    print("Runtime: ")
    print(datetime.now() - startTime) #write this out to a log file
    
def dbConnect():
    db = MySQLdb.connect("localhost","stonx_svc","pw","mysql")
    curs=db.cursor()
    curs.execute("select * from user")
    #Below loop is not needed for insert
    #dbResult = curs.fetchall()
    db.close()
    
def dbWrite(str):
    ticker = str
    print("Writing to database...")
    db = MySQLdb.connect("localhost","stonx_svc","pw","stonxTEST")
    curs=db.cursor()
    #Need insert statement with variables from csv file
    curs.execute("LOAD DATA LOCAL INFILE '/home/pi/Projects/stonx/Data/" + ticker + "_mod.csv' INTO table ticker_data FIELDS TERMINATED BY ','(historical_data_time,open,high,low,close,adj_close,volume,ticker)")
    #Below loop is not needed for insert
    #dbResult = curs.fetchall()
    db.commit()
    db.close()
    
def csvWrite(str):
    ticker = str
    print(ticker)
    with open('/home/pi/Projects/stonx/Data/' + ticker + '.csv', 'r') as read_obj, \
            open('/home/pi/Projects/stonx/Data/' + ticker + '_mod.csv', 'w', newline='') as write_obj:
        # Create a csv.reader object from the input file object
        csv_reader = reader(read_obj)
        # Create a csv.writer object from the output file object
        csv_writer = writer(write_obj)
        # Read each row of the input csv file as list
        for row in csv_reader:
        # Append the default text in the row / list
            row.append(ticker)
        # Add the updated row / list to the output file
            csv_writer.writerow(row)
    os.remove('/home/pi/Projects/stonx/Data/' + ticker + '.csv')

def main():
    #add for loop "for x in csv file"
    dataDownload(setTicker())
    csvWrite(setTicker())
    dbWrite(setTicker())
    runTime()
    exit()

main()
